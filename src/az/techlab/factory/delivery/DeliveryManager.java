package az.techlab.factory.delivery;

import az.techlab.PackageInfo;
import az.techlab.proxy.delivery.DeliveryProxyForBicycle;
import az.techlab.proxy.delivery.DeliveryProxyForCar;
import az.techlab.proxy.delivery.DeliveryProxyForTruck;

public class DeliveryManager {
    public String deliveryManagerFactory(PackageInfo packageInfo) {
        if(packageInfo.getSize().equals("SMALL")){
            return new DeliveryProxyForBicycle().deliveryWay(packageInfo);
        }
        if(packageInfo.getSize().equals("MEDIUM")){
            return new DeliveryProxyForCar().deliveryWay(packageInfo);
        }
        if(packageInfo.getSize().equals("LARGE")){
            return new DeliveryProxyForTruck().deliveryWay(packageInfo);
        }
        return "FAILED TO DELIVERY";
    }
}
