package az.techlab;

import az.techlab.factory.delivery.DeliveryManager;

public class ProxyPatternTest {
    public static void main(String[] args) {
        DeliveryManager deliveryManager = new DeliveryManager();
        PackageInfo packageInfoSmall = new PackageInfo("SMALL", 5);
        PackageInfo packageInfoMedium = new PackageInfo("MEDIUM", 12);
        PackageInfo packageInfoLarge = new PackageInfo("LARGE", 52);

        System.out.println(deliveryManager.deliveryManagerFactory(packageInfoSmall));
        System.out.println(deliveryManager.deliveryManagerFactory(packageInfoMedium));
        System.out.println(deliveryManager.deliveryManagerFactory(packageInfoLarge));
    }
}
