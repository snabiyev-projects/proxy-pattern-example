package az.techlab.service.delivery;

import az.techlab.PackageInfo;

public interface PackageDelivery {
    String deliveryWay(PackageInfo packageInfo);
}
