package az.techlab.service.delivery.imp;

import az.techlab.PackageInfo;
import az.techlab.service.delivery.PackageDelivery;

public class DeliveryByTruck implements PackageDelivery {
    @Override
    public String deliveryWay(PackageInfo packageInfo) {
        return "Delivery by truck. Package info: " + packageInfo;
    }
}
