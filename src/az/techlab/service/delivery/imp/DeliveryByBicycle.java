package az.techlab.service.delivery.imp;

import az.techlab.PackageInfo;
import az.techlab.service.delivery.PackageDelivery;

public class DeliveryByBicycle implements PackageDelivery {
    @Override
    public String deliveryWay(PackageInfo packageInfo) {
        return "Delivery by bicycle. Package info: " + packageInfo;
    }
}
