package az.techlab;

public class PackageInfo {
    private String size;
    private Integer weight;

    public PackageInfo(String size, Integer weight) {
        this.size = size;
        this.weight = weight;
    }

    public PackageInfo() {
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "PackageInfo{" +
                "size='" + size + '\'' +
                ", weight='" + weight + '\'' +
                '}';
    }
}
