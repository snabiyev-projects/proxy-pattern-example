package az.techlab.proxy.delivery;

import az.techlab.PackageInfo;
import az.techlab.service.delivery.imp.DeliveryByTruck;

public class DeliveryProxyForTruck extends DeliveryByTruck {
    @Override
    public String deliveryWay(PackageInfo packageInfo) {
        if (packageInfo.getWeight() > 50) {
            return "Package delivery for package: " + packageInfo + " is not possible by truck due overweight.";
        }
        return super.deliveryWay(packageInfo);
    }
}
