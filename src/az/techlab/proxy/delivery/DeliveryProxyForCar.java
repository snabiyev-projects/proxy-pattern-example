package az.techlab.proxy.delivery;

import az.techlab.PackageInfo;
import az.techlab.service.delivery.imp.DeliveryByCar;

public class DeliveryProxyForCar extends DeliveryByCar {
    @Override
    public String deliveryWay(PackageInfo packageInfo) {
        if (packageInfo.getWeight() > 10) {
            return "Package delivery for package: " + packageInfo + " is not possible by car due overweight.";
        }
        return super.deliveryWay(packageInfo);
    }
}
