package az.techlab.proxy.delivery;

import az.techlab.PackageInfo;
import az.techlab.service.delivery.imp.DeliveryByBicycle;

public class DeliveryProxyForBicycle extends DeliveryByBicycle {
    @Override
    public String deliveryWay(PackageInfo packageInfo) {
        if (packageInfo.getWeight() > 5) {
            return "Package delivery for package: " + packageInfo + " is not possible by bicycle due overweight.";
        }
        return super.deliveryWay(packageInfo);
    }
}
